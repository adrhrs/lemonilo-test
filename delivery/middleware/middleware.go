package middleware

import (
	"context"
	"encoding/json"
	model "lemonilo-test/model"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

func JWTAuthorization(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var header = r.Header.Get("x-access-token")
		header = strings.TrimSpace(header)
		if header == "" {
			w.WriteHeader(http.StatusForbidden)
			json.NewEncoder(w).Encode(model.Response{
				Message: "User is not authorized",
			})
			return
		}
		tk := &model.Account{}
		_, err := jwt.ParseWithClaims(header, tk, func(token *jwt.Token) (interface{}, error) {
			return []byte("secret"), nil
		})

		if err != nil {
			w.WriteHeader(http.StatusForbidden)
			json.NewEncoder(w).Encode(model.Response{Message: err.Error()})
			return
		}

		contextKey := "account"
		ctx := context.WithValue(r.Context(), contextKey, tk)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
