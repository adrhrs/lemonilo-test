package account

import (
	"encoding/json"
	"lemonilo-test/model"
	"net/http"

	repoRedisAccount "lemonilo-test/repository/account/redis"
	usecaseAccount "lemonilo-test/usecase/account"
)

func (d *DeliveryAccount) LoginHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	res := model.Response{}
	defer func() {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)
	}()

	acc := model.Account{}
	err := json.NewDecoder(r.Body).Decode(&acc)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		res.Message = "request payload is invalid"
		return
	}

	data, err := d.Usecase.Login(ctx, acc)
	if err != nil && (err == repoRedisAccount.ErrNotFound || err == usecaseAccount.ErrInvalidCredentials) {
		res.Message = err.Error()
		return
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		res.Message = "something went wrong"
		return
	}
	msg := data
	res.Data = msg
	res.Message = "login successful"
	return
}

func (d *DeliveryAccount) RegisterHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	res := model.Response{}
	defer func() {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)
	}()

	acc := model.Account{}
	err := json.NewDecoder(r.Body).Decode(&acc)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		res.Message = "request payload is invalid"
		return
	}

	if len(acc.Password) < 6 {
		res.Message = "password must be 6 character or more"
		return
	}

	data, err := d.Usecase.Register(ctx, acc)
	if err != nil && err == usecaseAccount.ErrEmailExist {
		res.Message = err.Error()
		return
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		res.Message = "something went wrong"
		return
	}
	msg := data
	res.Data = msg
	res.Message = "success register profile"
	return
}
