package account

import usecase "lemonilo-test/usecase/account"

type DeliveryAccount struct {
	Usecase *usecase.UsecaseAccount
}

func New(usecase *usecase.UsecaseAccount) *DeliveryAccount {
	return &DeliveryAccount{
		Usecase: usecase,
	}
}
