package account

import (
	"context"
	"encoding/json"
	"net/http"

	"lemonilo-test/model"
	repoRedisAccount "lemonilo-test/repository/account/redis"
)

func (d *DeliveryAccount) IndexHandler(w http.ResponseWriter, r *http.Request) {
	res := model.Response{}
	defer func() {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)
	}()

	data, err := d.Usecase.Index(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		res.Message = "something went wrong"
		return
	}
	msg := data
	res.Data = msg
	res.Message = "server is up"
	return
}

func (d *DeliveryAccount) GetProfileHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	res := model.Response{}
	defer func() {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)
	}()

	var account = *getAccountInfoFromContext(ctx)
	msg, err := d.Usecase.GetProfile(ctx, account)
	if err != nil && err == repoRedisAccount.ErrNotFound {
		res.Message = err.Error()
		return
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		res.Message = "something went wrong"
		return
	}
	res.Data = msg
	res.Message = "success get profile"
	return
}

func (d *DeliveryAccount) UpdateProfileHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	res := model.Response{}
	defer func() {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)
	}()

	updatedAccount := model.Account{}
	err := json.NewDecoder(r.Body).Decode(&updatedAccount)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		res.Message = "request payload is invalid"
		return
	}

	var account = *getAccountInfoFromContext(ctx)
	updatedAccount.Email = account.Email

	msg, err := d.Usecase.UpdateProfile(ctx, updatedAccount)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		res.Message = "something went wrong"
		return
	}
	res.Data = msg
	res.Message = "success update profile"
	return
}

func (d *DeliveryAccount) DeleteProfileHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	res := model.Response{}
	defer func() {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)
	}()

	var account = *getAccountInfoFromContext(ctx)
	err := d.Usecase.DeleteProfile(ctx, account)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		res.Message = "something went wrong"
		return
	}
	res.Data = account.Email
	res.Message = "success delete profile"
	return
}

func getAccountInfoFromContext(ctx context.Context) (account *model.Account) {
	account = ctx.Value("account").(*model.Account)
	return
}
