package redis

import (
	"github.com/go-redis/redis"
)

type Repo struct {
	Redis *redis.Client
}

func New(client *redis.Client) *Repo {
	return &Repo{
		Redis: client,
	}
}
