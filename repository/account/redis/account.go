package redis

import (
	"encoding/json"
	"errors"
	"lemonilo-test/model"
	"log"

	"github.com/go-redis/redis"
)

var (
	ErrNotFound = errors.New("user not exist")
)

func (r *Repo) GetAccount(key string) (account model.Account, err error) {
	value, err := r.Redis.HGet("secret", key).Result()
	if err != nil && err == redis.Nil {
		err = ErrNotFound
		return
	} else if err != nil {
		log.Println(err)
		return
	}
	err = json.Unmarshal([]byte(value), &account)
	if err != nil {
		log.Println(err)
		return
	}
	return
}

func (r *Repo) SetAccount(key string, account model.Account) error {
	acc, err := json.Marshal(account)
	if err != nil {
		log.Println(err)
		return err
	}

	_, err = r.Redis.HSet("secret", key, acc).Result()
	return err
}

func (r *Repo) DelAccount(key string) error {
	_, err := r.Redis.HDel("secret", key).Result()
	if err != nil && err == redis.Nil {
		err = ErrNotFound
		return err
	} else if err != nil {
		log.Println(err)
		return err
	}
	return err
}
