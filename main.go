package main

import (
	deliveryAccount "lemonilo-test/delivery/account"
	middleware "lemonilo-test/delivery/middleware"
	repoRedisAccount "lemonilo-test/repository/account/redis"
	usecaseAccount "lemonilo-test/usecase/account"
	"log"
	"net/http"

	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

var (
	APP_PORT = ":1111"
)

func initRouter(delivery deliveryAccount.DeliveryAccount) *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/ping", delivery.IndexHandler)
	router.HandleFunc("/api/v1/register", delivery.RegisterHandler).Methods("POST")
	router.HandleFunc("/api/v1/login", delivery.LoginHandler).Methods("POST")

	subRouter := router.PathPrefix("/api/v1/user").Subrouter()
	subRouter.Use(middleware.JWTAuthorization)
	subRouter.HandleFunc("/profile", delivery.GetProfileHandler).Methods("GET")
	subRouter.HandleFunc("/profile", delivery.UpdateProfileHandler).Methods("PUT")
	subRouter.HandleFunc("/profile", delivery.DeleteProfileHandler).Methods("DELETE")

	return router
}

func initConfig() (env map[string]string, err error) {
	err = godotenv.Load(".env")
	if err != nil {
		return
	}
	env = make(map[string]string)
	env, err = godotenv.Read()
	if err != nil {
		return
	}

	return

}

func initRedis(env map[string]string) *redis.Client {

	address := env["REDIS_PORT"]
	client := redis.NewClient(&redis.Options{
		Addr: address,
	})
	log.Println("successfully init Redis \t ", address)

	return client
}

func run() {

	config := make(map[string]string)
	config, err := initConfig()
	if err != nil {
		log.Println(err)
		return
	}

	redisClient := initRedis(config)

	repoRedisAccountObject := repoRedisAccount.New(redisClient)
	usecaseAccountObject := usecaseAccount.New(repoRedisAccountObject)
	deliveryAccountObject := deliveryAccount.New(usecaseAccountObject)

	router := initRouter(*deliveryAccountObject)

	log.Printf("Ready to rock and roll  \t  %s ", config["APP_PORT"])
	log.Fatal(http.ListenAndServe(APP_PORT, router))
}
func main() {
	run()
}
