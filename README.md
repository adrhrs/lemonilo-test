# lemonilo-technical-tes


To start this app, perform the following step in order

1. Clone this repo to your machine 
2. cd into the project folder
3. install redis if you don't have one
4. run `dep ensure -v --vendor-only .` to get dependencies
5. enter `make gobuildaccountservice` or `go run .`  to start server

## System Architecture
The design of this service is using `Clean Architecture` design (implemented on simpler version). Separating code to 4 layers, which are `models`, `delivery`, `usecase` and `repository`. Models layer storing information of the domain or entity that can be accessed from others layer.
Delivery layer has purpose as an "entry point" to business logic or we can call it `presenter layer`, this can be from http request, gRPC, cron and NSQ Consumer. Usecase layer has purpose to accomodate business logic and inside this layer can be separated to different modules depend on business need. 
Last one is repository layer or data access layer, for this layer is to read/write from any data point, for example querying to DB / Elasticsearch, request to external API, publish message to MQ or make a grpc call from another service. 

For data storage should be using database such as postgreSQL, but to simplify I'm using in-memory database (redis), for authentication method I'm using JWT based authentication

## Documentation 

This repository is serving several API for account management including register account, login, update account and delete account. Also the data currently storing on redis instead of database (for simplification purpose)

### Account Registration API
`/api/v1/register [POST]`

You need to add request payload something like this
```
{
    "name":"Wanda Maximoff",
    "email":"wanda@gmail.com",
    "password":"password123"
}
```
Email must be unique and password must be at least 6 character.

### Account Log In API
`/api/v1/login [POST]`

Log in need information of email and password
```
{
    "email":"wanda@gmail.com",
    "password":"password123"
}
```
you will receive `JWT Token` that need to be added on request header for authorized API

example response
```
{
    "message": "login successful",
    "data": {
        "name": "wanda",
        "email": "adrian@gmail.com",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJOYW1lIjoid2FuZGEiLCJFbWFpbCI6ImFkcmlhbkBnbWFpbC5jb20iLCJleHAiOjE2MTUxMjAzNjR9.SRo3KI7bTwWdLLySj5Z-5QXAyChKt0gWsjyyAtAc_nE"
    }
}
```

### Get User Profile API
`/api/v1/user/profile [GET]`

to use this API you need to login (see login API documentation above). Add new request header `x-access-token` with the value of `JWT Token` from login API response. This API will retrieve your profile information

example response
```
{
    "message": "success get profile",
    "data": {
        "name": "wanda",
        "email": "adrian@gmail.com"
    }
}
```

### Update User Profile API
`/api/v1/user/profile [PUT]`

to use this API you need to login (see login API documentation above). Add new request header `x-access-token` with the value of `JWT Token` from login API response. You can only change your name and password.

example request payload
```
{
    "name": "Tony Stark",
    "password": "kucingoren"
}
```

example response
```
{
    "message": "success update profile",
    "data": {
        "name": "Tony Stark",
        "email": "adrian@gmail.com"
    }
}
```

### Delete User Profile API
`/api/v1/user/profile [DELETE]`

to use this API you need to login (see login API documentation above). Add new request header `x-access-token` with the value of `JWT Token` from login API response. This API will delete your profile


example response
```
{
    "message": "success delete profile",
    "data": "adrian@gmail.com"
}
```

## References
https://hackernoon.com/golang-clean-archithecture-efd6d7c43047
https://codeburst.io/using-jwt-for-authentication-in-a-golang-application-e0357d579ce2
https://github.com/linux08/auth