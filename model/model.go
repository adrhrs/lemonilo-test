package model

import "github.com/dgrijalva/jwt-go"

type Account struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password,omitempty"`
	Token    string `json:"token,omitempty"`
	*jwt.StandardClaims
}

type Response struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}
