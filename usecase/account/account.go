package account

import (
	"context"
	"errors"
	"lemonilo-test/model"
	"log"
	"time"

	repoRedisAccount "lemonilo-test/repository/account/redis"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

var (
	ErrInvalidCredentials = errors.New("password is not match")
	ErrEmailExist         = errors.New("email has already been taken")
)

func (u *UsecaseAccount) Index(ctx context.Context) (string, error) {
	return "pong", nil
}

func (u *UsecaseAccount) Register(ctx context.Context, account model.Account) (model.Account, error) {

	_, err := u.RepoRedis.GetAccount(account.Email)
	if err != nil && err != repoRedisAccount.ErrNotFound {
		log.Println(err)
		return account, err
	} else if err == nil {
		err = ErrEmailExist
		return account, err
	}

	pass, err := bcrypt.GenerateFromPassword([]byte(account.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		return account, err
	}
	account.Password = string(pass)
	err = u.RepoRedis.SetAccount(account.Email, account)
	if err != nil {
		log.Println(err)
	}

	account.Password = ""

	return account, err
}

type Token struct {
	Name  string
	Email string
	*jwt.StandardClaims
}

func (u *UsecaseAccount) Login(ctx context.Context, account model.Account) (storedAccount model.Account, err error) {

	storedAccount, err = u.RepoRedis.GetAccount(account.Email)
	if err != nil {
		log.Println(err)
		return account, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(storedAccount.Password), []byte(account.Password))
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		err = ErrInvalidCredentials
		return account, err
	} else if err != nil {
		log.Println(err)
		return account, err
	}

	duration := time.Now().Add(time.Minute * 60).Unix()
	tkn := &Token{
		Name:  storedAccount.Name,
		Email: storedAccount.Email,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: duration,
		},
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tkn)
	tokenString, err := token.SignedString([]byte("secret"))
	if err != nil {
		log.Println(err)
	}

	storedAccount.Token = tokenString
	storedAccount.Password = ""

	return
}

func (u *UsecaseAccount) GetProfile(ctx context.Context, account model.Account) (storedAccount model.Account, err error) {

	storedAccount, err = u.RepoRedis.GetAccount(account.Email)
	if err != nil {
		log.Println(err)
		return account, err
	}
	storedAccount.Password = ""
	return

}

func (u *UsecaseAccount) UpdateProfile(ctx context.Context, account model.Account) (model.Account, error) {
	pass, err := bcrypt.GenerateFromPassword([]byte(account.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		return account, err
	}
	account.Password = string(pass)
	err = u.RepoRedis.SetAccount(account.Email, account)
	if err != nil {
		log.Println(err)
	}

	account.Password = ""
	return account, err
}

func (u *UsecaseAccount) DeleteProfile(ctx context.Context, account model.Account) (err error) {

	err = u.RepoRedis.DelAccount(account.Email)
	if err != nil {
		log.Println(err)
		return err
	}
	return

}
