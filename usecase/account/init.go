package account

import (
	repoRedisAccount "lemonilo-test/repository/account/redis"
)

type UsecaseAccount struct {
	RepoRedis *repoRedisAccount.Repo
}

func New(repoRedis *repoRedisAccount.Repo) *UsecaseAccount {
	return &UsecaseAccount{
		RepoRedis: repoRedis,
	}
}
